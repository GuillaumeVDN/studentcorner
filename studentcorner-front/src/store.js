import Vue from 'vue';
import Vuex from 'vuex';

import TabHome from "@/components/tabs/TabHome";
import TabWhyJoin from "@/components/tabs/TabWhyJoin";
import TabClubs from "@/components/tabs/TabClubs";
import TabSocieties from "@/components/tabs/TabSocieties";
import TabPublications from "@/components/tabs/TabPublications";
import TabChat from "@/components/tabs/TabChat";

import utils_network from "@/utils/utils_network";

Vue.use(Vuex);

const tabs = {
    home: { key: 'home', route: '/', component: TabHome, icon: 'home', label: "Home" },
    whyjoin: { key: 'whyjoin', route: '/whyjoin', component: TabWhyJoin, icon: 'question-circle', label: "Why join?" },
    clubs: { key: 'clubs', route: '/clubs', component: TabClubs, icon: 'user-friends', label: "Clubs" },
    societies: { key: 'societies', route: '/societies', component: TabSocieties, icon: 'users', label: "Societies" },
    publications: { key: 'publications', route: '/publications', component: TabPublications, icon: 'newspaper', label: "News/Events" },
    chat: { key: 'chat', route: '/chat', component: TabChat, icon: 'comment-dots', label: "What's up?" }
};

const initialState = {
    /** Global variables */
    tabs,
    groupPageRoute: '/group/:groupId/:pageKey',
    groupPageRouteNoPage: '/group/:groupId',
    studentPageRoute: '/student/:studentId',

    /** User that is currently logged in */
    user: null,

    /** Util variable to detect changes */
    createdPublications: 0,
    createdJoinRequest: 0
}

const mutations = {
    setUser(state, { user }) {
        state.user = user;
    },
    addCreatedPublication(state) {
        ++state.createdPublications;
    },
    addCreatedJoinRequest(state) {
        ++state.createdJoinRequest;
    }
}

const getters = {  // like computed properties ; no need to use that just to make a simple getter
};

const actions = {

    // ------------------------------ USER/AUTH ------------------------------

    attemptLoginFromCredentials(context, { email, password, description }) {
        return utils_network.apiPost('/auth', { 'email': email, 'password': password, 'description': description })
            .then(value => {
                window.localStorage.setItem('token', value.token);
            })
            .then(() => context.dispatch('attemptLoginFromToken'));
    },
    attemptLoginFromToken(context) {
        if (window.localStorage.getItem('token')) {
            return utils_network.apiGet('/students/@me')
                .then(user => context.commit('setUser', { user }));
        }
    },
    attemptLogout(context) {
        // remove data
        context.commit('setUser', { user: null });

        // remove token from remote and from cookies
        return utils_network.apiDelete('/auth')
            .finally(() => {
                window.localStorage.removeItem('token');
            });
    },
    attemptSignup(context, { firstName, lastName, email, password }) {
        return utils_network.apiPost('/students', { 'firstName': firstName, 'lastName': lastName, 'email': email, 'password': password });
    }

}

export default new Vuex.Store({
    state: initialState,
    getters,
    actions,
    mutations
});
