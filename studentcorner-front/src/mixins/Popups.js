import utils_modal from "@/utils/utils_modal";

import ModalLogin from '@/components/account/ModalLogin'
import ModalSignup from '@/components/account/ModalSignup'
import ModalCreatePublication from "@/components/publications/ModalCreatePublication";
import ModalGroupJoinRequest from "@/components/groups/ModalGroupJoinRequest";

export default {
    methods: {
        openLogin() {
            utils_modal.openModal(this, ModalLogin);
        },
        openSignup() {
            utils_modal.openModal(this, ModalSignup);
        },
        openCreatePublication(group) {
            utils_modal.openModal(this, ModalCreatePublication, { group });
        },
        openGroupJoinRequest(group) {
            utils_modal.openModal(this, ModalGroupJoinRequest, { group });
        }
    }
}
