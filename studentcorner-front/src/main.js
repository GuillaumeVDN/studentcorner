import Vue from 'vue';
import VueRouter from 'vue-router'
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import App from '@/components/App.vue';
import GroupPage from "@/components/groups/GroupPage";
import TabStudentPage from "@/components/tabs/TabStudentPage";
import store from '@/store'

// setup buefy
Vue.use(Buefy, { defaultIconPack: 'fas' });
import '@/scss/style.scss';

// setup router
const routes = [];
for (let tab of Object.values(store.state.tabs)) {
    routes.push({ path: tab.route, component: tab.component })
}
routes.push({ path: store.state.groupPageRoute, component: GroupPage })
routes.push({ path: store.state.groupPageRouteNoPage, redirect: store.state.groupPageRoute.replace(':pageKey', 'home') })
routes.push({ path: store.state.studentPageRoute, component: TabStudentPage })
routes.push({ path: '/:catchAll(.*)', component: store.state.tabs.home.component });

Vue.use(VueRouter);
const router = new VueRouter({
    routes
});

// start app
export default new Vue({
    el: 'body',
    store,
    router,
    render: h => h(App)
});

// login from token
store.dispatch('attemptLoginFromToken');
