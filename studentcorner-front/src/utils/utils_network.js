import app from "@/main";

const errors = {
	'not found': 'The required content could not be found.',
	'unknown error': 'A unknown error occurred.',
	'invalid request': 'An invalid request was made to the server.',
	'invalid token': 'Your session is invalid, you were logged out.',
	'unknown user': 'Unknown e-mail.',
	'invalid credentials': 'Invalid password.',
	'insufficient permissions': 'You don\'t have the permissions to do that.',
	'user already exists': 'An account with this e-mail already exists.'
};

export default {

	// ------------------------------ API CALLS ------------------------------

	apiFetch(route, method, body) {
		// set route
		route = '/api' + route
		if (window.webpackHotUpdate) {
			route = `http://localhost:26384${route}`
		}
		// build params
		let params = {
			'headers': {
				'Authorization': window.localStorage.getItem('token')
			},
			'method': method
		};
		if (body) {
			params.headers['Content-Type'] = 'application/json'
			params.body = JSON.stringify(body)
		}
		return fetch(route, params)
			.then(res => res.json())
			.catch(error => {
				throw { 'message': 'Couldn\'t ' + method + ' ' + route + ' : ' + error.message };
			})
			.then(json => {
				if (json.error) return Promise.reject({ 'message': json.error });
				return json.value;
			})
	},
	apiGet(route) {
		return this.apiFetch(route, 'GET')
			.then(value => {
				if (value) return value;
				return Promise.reject({ 'message': 'empty response in GET from ' + route });
			})
			.catch(this.handleError)
	},
	apiPost(route, body) {
		return this
			.apiFetch(route, 'POST', body)
			.catch(this.handleError);
	},
	apiPatch(route, body) {
		return this
			.apiFetch(route, 'PATCH', body)
			.catch(this.handleError);
	},
	apiDelete(route, body) {
		return this
			.apiFetch(route, 'DELETE', body)
			.catch(this.handleError);
	},

	// ------------------------------ ERROR HANDLING ------------------------------

	handleError(error) {
		// display error anyway
		let msg = error.message;
		if (msg.includes('Failed to fetch')) {
			msg = 'Could not communicate with server.';
		} else {
			for (let [key, value] of Object.entries(errors)) {
				if (error.message.startsWith(key)) {
					msg = value;
					break;
				}
			}
		}
		// error toast
		app.$buefy.toast.open({ 'duration': 2000, 'message': msg, 'type': 'is-danger' });
		// force disconnect if invalid token
		if (error.message === 'invalid token') {
			app.$store.dispatch('attemptLogout').then();
		}
		// do not run future code ; log error (uncaught promise)
		if (error.message && !error.message.includes('Handled error')) {
			error.message = 'Handled error (rejected promise)' + (error.message ? ' : ' + error.message : '');
		}
		return Promise.reject(error);
	}

}
