import app from "@/main";

export default {

	openModal(popups, comp, props = {}) {
		app.$buefy.modal.open({
			parent: popups,
			component: comp,
			hasModalCard: true,
			trapFocus: true,
			props
		});
	},
	showInfoToast(message) {
		this.showToast(5000, message, 'is-success', 'is-bottom-left');
	},
	showImportantInfoToast(message) {
		this.showToast(10000, message, 'is-danger', 'is-top-left');
	},

	lastToast: null,
	showToast(duration, message, type, position) {
		if (this.lastToast) {
			this.lastToast.close();
		}
		this.lastToast = app.$buefy.toast.open({
			'duration': duration,
			'message': message,
			'type': type,
			'position': position,
			'queue': true
		});
	}

}
