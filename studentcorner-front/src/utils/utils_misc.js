import dayjs from "dayjs";

let showdown = require('showdown');
let showdownHtmlEscape = require('showdown-htmlescape')
let showdownConverter = new showdown.Converter({ extensions: [showdownHtmlEscape] })
showdownConverter.setFlavor('github');
showdownConverter.setOption('simpleLineBreaks', true);
showdownConverter.setOption('literalMidWordUnderscores', true);
showdownConverter.setOption('literalMidWordAsterisks', true);
showdownConverter.setOption('disableForced4SpacesIndentedSublists', true);
showdownConverter.setOption('openLinksInNewWindow', true);
showdownConverter.setOption('underline', true);

export default {

	formatDateTime(date) {
		return dayjs(date).format("dddd, MMMM DD YYYY, [at] hh:mm A");
	},
	formatTime(date) {
		return dayjs(date).format("hh:mm A");
	},
	markdownToHtml(markdown) {
		return showdownConverter.makeHtml(markdown.replaceAll('_', '@UNDERSCORE@')).replaceAll('@UNDERSCORE@', '_');
	},

	// https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
	randomString(length) {
		let result = '';
		let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		let charactersLength = characters.length;
		for (let i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() *
				charactersLength));
		}
		return result;
	}

}
