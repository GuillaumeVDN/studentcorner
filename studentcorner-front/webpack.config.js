let path = require("path");
const createExpoWebpackConfigAsync = require("@expo/webpack-config");

const aliases = {
    '@': path.resolve(__dirname, '/src/'),
};

module.exports = async function (env, argv) {
    env.mode = "development"
    const config = await createExpoWebpackConfigAsync(env, argv);

    config.resolve.alias = {
        ...aliases,
        ...config.resolve.alias,
    };

    return config
};
