# StudentCorner

A full-stack web application allowing university students to connect with their local clubs and organizations.
See screenshots under `/screenshots/` to get a general idea of the features.

Made using vue.js, Buefy, express.js and MySQL, in the context of a course project for Staffordshire University with two other students.
