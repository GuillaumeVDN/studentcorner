const express = require('express');
const utils = require('../utils');
const dayjs = require("dayjs");

let router = express.Router();

// get group permissions for authenticated student
router.get('/@me/bygroup/:groupId', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT isMember, canPostNews, canEditPermissions
                          FROM groups_permissions p
                          JOIN students s ON studentId = s.id
                     WHERE groupId = ${parseInt(req.params.groupId)}
                       AND studentId = ${student.id};
        `;
        utils.dbQueryResultFirst(res, query, permissions => {
            utils.resValue(res, {
                'groupId': parseInt(req.params.groupId),
                'studentId': student.id,
                'isMember': permissions.isMember === 1,
                'canPostNews': permissions.canPostNews === 1,
                'canEditPermissions': permissions.canEditPermissions === 1
            });
        }, () => resultDefaultPermissions(res));
    }, () => resultDefaultPermissions(res));
});
function resultDefaultPermissions(res) {
    utils.resValue(res, {
        'isMember': false,
        'canPostNews': false,
        'canEditPermissions': false
    });
}

// update permissions for a user
router.patch('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "groupId", "studentId", "isMember", "canPostNews", "canEditPermissions", (groupId, studentId, isMember, canPostNews, canEditPermissions) => {
            let query = `SELECT canEditPermissions
                          FROM groups_permissions p
                     WHERE groupId = ${parseInt(groupId)}
                       AND studentId = ${student.id};
            `;
            console.log(query);
            utils.dbQueryResultFirst(res, query, permissions => {
                if (permissions.canEditPermissions === 1) {
                    let query = `INSERT INTO groups_permissions(groupId, studentId, isMember, canPostNews, canEditPermissions)
                                           VALUES(${parseInt(groupId)}, ${parseInt(studentId)}, ${isMember ? 1 : 0},
                                                  ${canPostNews ? 1 : 0}, ${canEditPermissions ? 1 : 0})
                                           ON DUPLICATE KEY UPDATE isMember = VALUES(isMember),
                                                                   canPostNews = VALUES(canPostNews),
                                                                   canEditPermissions = VALUES(canEditPermissions)
                                           ;
                    `;
                    console.log(query);
                    utils.dbQueryUpdate(res, query, () => {
                        utils.resValue(res);
                    });
                } else {
                    utils.resError(res, 401, "no permission");
                }
            }, () => utils.resError(res, 401, "no permission"));
        });
    });
});

// export module
module.exports = router;
