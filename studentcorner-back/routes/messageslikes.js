const express = require('express');
const utils = require('../utils');
const dayjs = require("dayjs");

let router = express.Router();

// get all likes for a message
router.get('/byid/:messageId', (req, res) => {
    let query = `SELECT studentId
                 FROM groups_messages_likes
                 WHERE messageId = ${parseInt(req.params.messageId)};
    `;
    utils.dbQueryResult(res, query, likes => {
        let list = [];
        for (let queryResultObject of likes) {
            list.push({
                studentId: queryResultObject.studentId
            });
        }
        utils.resValue(res, list);
    });
});

// get all received likes for a user
router.get('/received/bystudent/:studentId', (req, res) => {
    let query = `SELECT messageId, l.studentId
                 FROM groups_messages_likes l
                 LEFT JOIN groups_messages m ON messageId = m.id
                 WHERE m.studentId = ${parseInt(req.params.studentId)};
    `;
    utils.dbQueryResult(res, query, likes => {
        console.log(likes);
        let list = [];
        for (let queryResultObject of likes) {
            list.push({
                messageId: queryResultObject.messageId,
                studentId: queryResultObject.studentId
            });
        }
        utils.resValue(res, list);
    });
});

// like a message as the authentified student
router.post('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "messageId", messageId => {
            let query = `INSERT INTO groups_messages_likes(messageId, studentId)
                                           VALUES(${parseInt(messageId)}, ${student.id});
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// unlike a message as the authentified student
router.delete('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "messageId", messageId => {
            let query = `DELETE FROM groups_messages_likes
                         WHERE messageId = ${parseInt(messageId)}
                           AND studentId = ${student.id};
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// export module
module.exports = router;
