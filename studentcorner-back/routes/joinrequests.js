const express = require('express');
const utils = require('../utils');
const dayjs = require("dayjs");

let router = express.Router();

// get all join requests for a group
router.get('/bygroup/:groupId', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT canEditPermissions
                     FROM groups_permissions
                     WHERE groupId = ${parseInt(req.params.groupId)}
                       AND studentId = ${student.id};
        `;
        utils.dbQueryResultFirst(res, query, permissions => {
            if (permissions.canEditPermissions === 1) {
                let query = `SELECT studentId
                             FROM groups_join_requests
                             WHERE groupId = ${parseInt(req.params.groupId)};
                `;
                utils.dbQueryResult(res, query, requests => {
                    let list = [];
                    for (let queryResultObject of requests) {
                        list.push({
                            studentId: queryResultObject.studentId,
                            motivation: queryResultObject.motivation
                        });
                    }
                    utils.resValue(res, list);
                });
            } else {
                utils.resError(res, 401, "no permission");
            }
        }, () => utils.resError(res, 401, "no permission"));
    });
});

// get join request for a group as the authentified student
router.get('/@me/bygroup/:groupId', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT studentId
                     FROM groups_join_requests
                     WHERE groupId = ${parseInt(req.params.groupId)}
                       AND studentId = ${student.id};
        `;
        utils.dbQueryResultFirst(res, query, request => {
            utils.resValue(res, {
                exists: true,
                motivation: request.motivation
            });
        }, () => utils.resValue(res, { exists: false }));
    }, () => utils.resValue(res, { exists: false }));
});

// create a join request as the authentified student
router.post('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "groupId", "motivation", (groupId, motivation) => {
            let query = `INSERT INTO groups_join_requests(groupId, studentId, motivation)
                                                   VALUES(${parseInt(groupId)}, ${student.id}, ${dbSafeize(motivation)})
                         ON DUPLICATE KEY UPDATE motivation = VALUES(motivation);
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// delete a join request as the authentified student
router.delete('/@me', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "groupId", groupId => {
            let query = `DELETE FROM groups_join_requests
                         WHERE groupId = ${parseInt(groupId)}
                           AND studentId = ${student.id};
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// delete a join request
router.delete('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "groupId", "studentId", (groupId, studentId) => {
            let query = `SELECT canEditPermissions
                         FROM groups_permissions
                         WHERE groupId = ${parseInt(groupId)}
                           AND studentId = ${parseInt(student.id)};
            `;
            utils.dbQueryResultFirst(res, query, permissions => {
                if (permissions.canEditPermissions === 1) {
                    let query = `DELETE FROM groups_join_requests
                                 WHERE groupId = ${parseInt(groupId)}
                                   AND studentId = ${parseInt(studentId)};
                    `;
                    utils.dbQueryUpdate(res, query, () => {
                        utils.resValue(res);
                    });
                } else {
                    utils.resError(res, 401, "no permission");
                }
            }, () => utils.resError(res, 401, "no permission"));
        });
    });
});

// export module
module.exports = router;
