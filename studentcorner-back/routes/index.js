const express = require('express');
const router = express.Router();
const path = require('path');

require("fs").readdirSync(__dirname).forEach(function(file) {
    if (file !== 'index.js') {
        router.use('/' + file.replace('.js', ''), require(path.join(__dirname, file)));
    }
});

router.use(function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader("Access-Control-Allow-Headers", "*");
    res.end();
});

module.exports = router;
