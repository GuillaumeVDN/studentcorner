const express = require('express');
const utils = require('../utils');
const dayjs = require('dayjs');

let router = express.Router();

function packStudentData(student) {
    return {
        'id': student.id,
        'firstName': student.firstName,
        'lastName': student.lastName,
        'email': student.email,
        'accountCreationDate': student.accountCreationDate
    };
}

// get all students
router.get('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT id, firstName, lastName, email, accountCreationDate
                     FROM students
                     ORDER BY lastName ASC, firstName ASC;
        `;
        utils.dbQueryResult(res, query, result => {
            let list = [];
            for (let queryResultObject of result) {
                list.push(packStudentData(queryResultObject));
            }
            utils.resValue(res, list);
        });
    });
});

// get all students with permissions for a group
router.get('/withpermissions/bygroup/:groupId', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT id, firstName, lastName, email, accountCreationDate,
                            COALESCE(isMember, 0) as isMember,
                            COALESCE(canPostNews, 0) as canPostNews,
                            COALESCE(canEditPermissions, 0) as canEditPermissions
                     FROM students
                     LEFT JOIN groups_permissions p ON id = p.studentId
                                                    AND groupId = ${parseInt(req.params.groupId)}
                     ORDER BY lastName ASC, firstName ASC;
        `;
        utils.dbQueryResult(res, query, result => {
            let list = [];
            for (let queryResultObject of result) {
                let obj = packStudentData(queryResultObject);
                obj.isMember = queryResultObject.isMember === 1;
                obj.canPostNews = queryResultObject.canPostNews === 1;
                obj.canEditPermissions = queryResultObject.canEditPermissions === 1;
                list.push(obj);
            }
            utils.resValue(res, list);
        });
    });
});

// get all students with join requests for a group
router.get('/withjoinrequests/bygroup/:groupId', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        let query = `SELECT id, firstName, lastName, email, accountCreationDate, motivation
                     FROM students
                     LEFT JOIN groups_join_requests j ON id = j.studentId
                                                      AND groupId = ${parseInt(req.params.groupId)}
                     ORDER BY lastName ASC, firstName ASC;
        `;
        utils.dbQueryResult(res, query, result => {
            let list = [];
            for (let queryResultObject of result) {
                let obj = packStudentData(queryResultObject);
                if (queryResultObject.motivation) {
                    obj.exists = true;
                    obj.motivation = queryResultObject.motivation;
                } else {
                    obj.exists = false;
                }
                list.push(obj);
            }
            utils.resValue(res, list);
        });
    });
});

// get data for authenticated student
router.get('/@me', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.resValue(res, packStudentData(student));
    });
});

// get data a student by id
router.get('/byid/:studentId', (req, res) => {
    let query = `SELECT *
                 FROM students
                 WHERE id = ${parseInt(req.params.studentId)}
    `;
    utils.dbQueryResultFirst(res, query, student => {
        utils.resValue(res, packStudentData(student));
    });
});

// create student
router.post('/', (req, res) => {
    utils.reqRequireParams(req, res, 'firstName', 'lastName', 'email', 'password', (firstName, lastName, email, password) => {
        // ensure doesn't already exist
        let query = `SELECT * FROM students
                     WHERE email = ${dbSafeize(email)};
        `;
        utils.dbQueryResultFirst(res, query, existing => {
            utils.resError(res, 409, 'user already exists');
        }, () => {
            // hash password and insert in db
            utils.hash(res, password, hash => {
                let query = `INSERT INTO students(firstName, lastName, email, passwordHash, accountCreationDate)
                                           VALUES(${dbSafeize(firstName)}, ${dbSafeize(lastName)}, ${dbSafeize(email)}, ${dbSafeize(hash)}, ${dayjs().valueOf()});
                `;
                utils.dbQueryUpdate(res, query, () => {
                    let query = `SELECT * FROM students
                        WHERE email = ${dbSafeize(email)};
                    `;
                    utils.dbQueryResultFirst(res, query, student => {
                        utils.resValue(res, {
                            'studentId': student.id
                        });
                    });
                });
            });
        });
    });
});

// export module
module.exports = router;
