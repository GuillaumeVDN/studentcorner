const express = require('express');
const utils = require('../utils');
const crypto = require("crypto");

let router = express.Router();

function packGroupData(queryResultObject) {
    return {
        id: queryResultObject.id,
        type: queryResultObject.type,
        name: queryResultObject.name,
        pitch: queryResultObject.pitch,
        description: queryResultObject.description,
        logo: queryResultObject.logo
    }
}

function getAndReturnGroups(res, query) {
    utils.dbQueryResult(res, query, groups => {
        let list = [];
        for (let group of groups) {
            list.push(packGroupData(group));
        }
        utils.resValue(res, list);
    });
}

// get all groups
router.get('/', (req, res) => {
    let query = `SELECT * FROM ${dbEscape}groups${dbEscape}
                 ORDER BY name ASC;
    `;
    getAndReturnGroups(res, query);
});

// get all groups of a certain type
router.get(`/bytype/:type`, (req, res) => {
    let query = `SELECT * FROM ${dbEscape}groups${dbEscape}
                 WHERE type = ${dbSafeize(req.params.type)}
                 ORDER BY name ASC;
        `;
    getAndReturnGroups(res, query);
});

// get all groups with a certain student being a member
router.get(`/withmember/:studentId`, (req, res) => {
    let query = `SELECT *
                 FROM ${dbEscape}groups${dbEscape}
                 JOIN groups_permissions p ON id = p.groupId
                 WHERE studentId = ${parseInt(req.params.studentId)}
                   AND isMember = 1
                 ORDER BY name ASC;
    `;
    getAndReturnGroups(res, query);
});

// get group by id
router.get(`/byid/:id`, (req, res) => {
    let query = `SELECT * FROM ${dbEscape}groups${dbEscape}
                 WHERE id = ${parseInt(req.params.id)};
    `;
    utils.dbQueryResultFirst(res, query, group => {
        utils.resValue(res, packGroupData(group));
    });
});

// export module
module.exports = router;
