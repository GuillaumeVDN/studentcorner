const express = require('express');
const utils = require('../utils');
const dayjs = require('dayjs');

let router = express.Router();

function packChatMessageData(queryResultObject) {
    return {
        id: queryResultObject.id,
        studentId: queryResultObject.studentId,
        message: queryResultObject.message,
        date: queryResultObject.date,
        studentFirstName: queryResultObject.firstName,
        studentLastName: queryResultObject.lastName
    }
}

function getAndReturnChatMessages(res, query) {
    utils.dbQueryResult(res, query, result => {
        let list = [];
        for (let queryResultObject of result) {
            list.push(packChatMessageData(queryResultObject));
        }
        utils.resValue(res, list);
    });
}

// get all chat messages between two dates
router.get('/:minDate/:maxDate', (req, res) => {
    let minDate = dayjs(parseInt(req.params.minDate));
    let maxDate = dayjs(parseInt(req.params.maxDate));
    let query = `SELECT c.id, studentId, message, date, firstName, lastName
                      FROM chat_messages c
                      JOIN students s ON studentId = s.id
                 WHERE ${dbEscape}date${dbEscape} >= ${dbSafeize(minDate.valueOf())}
                   AND ${dbEscape}date${dbEscape} <= ${dbSafeize(maxDate.valueOf())}
             ORDER BY date ASC;
    `;
    getAndReturnChatMessages(res, query);
});

// post chat message
router.post('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "message", message => {
            let query = `INSERT INTO chat_messages(studentId, message, date)
                                           VALUES(${student.id}, ${dbSafeize(message)}, ${dbSafeize(dayjs().valueOf())});
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// export module
module.exports = router;
