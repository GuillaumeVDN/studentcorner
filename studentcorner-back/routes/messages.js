const express = require('express');
const utils = require('../utils');
const dayjs = require("dayjs");

let router = express.Router();

function packMessageData(queryResultObject) {
    return {
        id: queryResultObject.id,
        groupId: queryResultObject.groupId,
        studentId: queryResultObject.studentId,
        message: queryResultObject.message,
        date: queryResultObject.date,
        studentFirstName: queryResultObject.firstName,
        studentLastName: queryResultObject.lastName
    }
}

function getAndReturnMessages(res, query) {
    utils.dbQueryResult(res, query, result => {
        let list = [];
        for (let queryResultObject of result) {
            list.push(packMessageData(queryResultObject));
        }
        utils.resValue(res, list);
    });
}

// get all messages for a group
router.get('/bygroup/:groupId/:type', (req, res) => {
    let query = `SELECT p.id, studentId, message, date, firstName, lastName
                          FROM groups_messages p
                          JOIN students s ON studentId = s.id
                 WHERE groupId = ${parseInt(req.params.groupId)}
                   AND type = ${dbSafeize(req.params.type)}
                 ORDER BY date DESC;
    `;
    getAndReturnMessages(res, query);
});

// post message
router.post('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "type", "groupId", "message", (type, groupId, message) => {
            let query = `INSERT INTO groups_messages(type, groupId, studentId, message, date)
                                           VALUES(${dbSafeize(type)}, ${parseInt(groupId)}, ${student.id}, ${dbSafeize(message)}, ${dbSafeize(dayjs().valueOf())});
            `;
            utils.dbQueryUpdate(res, query, () => {
                utils.resValue(res);
            });
        });
    });
});

// export module
module.exports = router;
