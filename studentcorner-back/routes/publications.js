const express = require('express');
const utils = require('../utils');
const crypto = require("crypto");
const dayjs = require("dayjs");

let router = express.Router();

function packPublicationData(queryResultObject) {
    return {
        id: queryResultObject.id,
        groupId: queryResultObject.groupId,
        studentId: queryResultObject.studentId,
        title: queryResultObject.title,
        message: queryResultObject.message,
        date: queryResultObject.date,
        groupName: queryResultObject.name,
        studentFirstName: queryResultObject.firstName,
        studentLastName: queryResultObject.lastName
    }
}

function getAndReturnPublications(res, query) {
    utils.dbQueryResult(res, query, result => {
        let list = [];
        for (let queryResultObject of result) {
            list.push(packPublicationData(queryResultObject));
        }
        utils.resValue(res, list);
    });
}

// get all publications
router.get('/', (req, res) => {
    let query = `SELECT p.id, groupId, studentId, title, message, date, name, firstName, lastName
                          FROM groups_publications p
                          JOIN ${dbEscape}groups${dbEscape} g ON groupId = g.id
                          JOIN students s ON studentId = s.id
                 ORDER BY date DESC;
    `;
    getAndReturnPublications(res, query);
});

// get all publications for a group
router.get('/bygroup/:groupId', (req, res) => {
    let query = `SELECT p.id, groupId, studentId, title, message, date, name, firstName, lastName
                          FROM groups_publications p
                          JOIN ${dbEscape}groups${dbEscape} g ON groupId = g.id
                          JOIN students s ON studentId = s.id
                 WHERE groupId = ${parseInt(req.params.groupId)}
                 ORDER BY date DESC;
    `;
    getAndReturnPublications(res, query);
});

// post publication
router.post('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, student => {
        utils.reqRequireParams(req, res, "groupId", "title", "message", (groupId, title, message) => {
            let query = `SELECT canPostNews
                          FROM groups_permissions p
                     WHERE groupId = ${parseInt(groupId)}
                       AND studentId = ${student.id};
            `;
            utils.dbQueryResultFirst(res, query, permissions => {
                if (permissions.canPostNews === 1) {
                    let query = `INSERT INTO groups_publications(groupId, studentId, title, message, date)
                                           VALUES(${parseInt(groupId)}, ${student.id}, ${dbSafeize(title)}, ${dbSafeize(message)}, ${dbSafeize(dayjs().valueOf())});
                    `;
                    utils.dbQueryUpdate(res, query, () => {
                        utils.resValue(res);
                    });
                } else {
                    utils.resError(res, 401, "no permission");
                }
            }, () => utils.resError(res, 401, "no permission"));
        });
    });
});

// export module
module.exports = router;
