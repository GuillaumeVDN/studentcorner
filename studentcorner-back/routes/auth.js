const express = require('express');
const crypto = require('crypto');
const utils = require('../utils');

let router = express.Router();

// create auth token
router.post('/', (req, res) => {
    utils.reqRequireParams(req, res, 'email', 'password', 'description', (email, password, description) => {
        // check email and password
        let query = `SELECT * FROM students
                     WHERE email = ${dbSafeize(email)};
        `;
        utils.dbQueryResultFirst(res, query, student => {
            utils.requireValidHash(res, password, student.passwordHash, () => {
                // create token
                let newToken = crypto.randomBytes(20).toString('hex');
                let query = `INSERT INTO students_auth(studentId, token, description)
                                                VALUES(${student.id}, ${dbSafeize(newToken)}, ${dbSafeize(description)});
                `;
                utils.dbQueryUpdate(res, query, () => {
                    utils.resValue(res, {
                        'studentId': student.id,
                        'token': newToken
                    });
                });
            });
        });
    });
});

// delete auth token
router.delete('/', (req, res) => {
    utils.reqRequireAuthentication(req, res, (student, token) => {
        let query = `DELETE FROM students_auth
                     WHERE token = ${dbSafeize(token)};
        `;
        utils.dbQueryUpdate(res, query, () => {
            utils.resValue(res);
        });
    });
});

// export module
module.exports = router;
