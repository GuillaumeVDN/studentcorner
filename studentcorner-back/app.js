// settings
const MYSQL_URL = 'localhost';
const MYSQL_USER = 'root';
const MYSQL_PWD = 'mysql';
const MYSQL_NAME = 'studentcorner';
const SERVER_PORT = 26384;

const express = require('express');
const mysql = require('mysql');
const path = require('path');
const logger = require('morgan');
const utils = require('./utils');

// app
const app = express();
app.use(logger('dev'));
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ limit: '100mb', extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

// easy CORS setup (don't bother with that, it's not *that* much of a secure project...)
let cors = require('cors');
let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
}
app.use(cors(corsOptions));

// connect to db and start
dbEscape = '`';
dbConnection = mysql.createConnection({ host: MYSQL_URL, user: MYSQL_USER, password: MYSQL_PWD, multipleStatements: true });
dbSafeize = text => dbConnection.escape(text);
dbConnection.connect(err => {
    if (err) throw err;
    console.log('Connected to MySQL');

    // create database and init tables
    let query = `
        CREATE DATABASE IF NOT EXISTS ${MYSQL_NAME};
        USE ${MYSQL_NAME};
        
        CREATE TABLE IF NOT EXISTS students (
            id INTEGER AUTO_INCREMENT,
            firstName VARCHAR(30),
            lastName VARCHAR(30),
            email VARCHAR(100),
            passwordHash VARCHAR(100),
            accountCreationDate BIGINT,
            PRIMARY KEY(id)
        );
        
        CREATE TABLE IF NOT EXISTS students_auth (
            studentId INTEGER,
            token CHAR(40),
            description VARCHAR(500),
            PRIMARY KEY(studentId, token)
        );
        
        CREATE TABLE IF NOT EXISTS ${dbEscape}groups${dbEscape} (
            id INTEGER AUTO_INCREMENT,
            type VARCHAR(10),
            name VARCHAR(100),
            pitch VARCHAR(250),
            description VARCHAR(1000),
            logo LONGTEXT,
            PRIMARY KEY(id)
        );
        
        CREATE TABLE IF NOT EXISTS groups_permissions (
            groupId INTEGER,
            studentId INTEGER,
            isMember BOOL,
            canPostNews BOOL,
            canEditPermissions BOOL,
            PRIMARY KEY(groupId, studentId)
        );
        
        CREATE TABLE IF NOT EXISTS groups_publications (
            id INTEGER AUTO_INCREMENT,
            groupId INTEGER,
            studentId INTEGER,
            title VARCHAR(100),
            message VARCHAR(250),
            date BIGINT,
            PRIMARY KEY(id)
        );
        
        CREATE TABLE IF NOT EXISTS groups_messages (
            id INTEGER AUTO_INCREMENT,
            type VARCHAR(10),
            groupId INTEGER,
            studentId INTEGER,
            message VARCHAR(250),
            date BIGINT,
            PRIMARY KEY(id)
        );
        
        CREATE TABLE IF NOT EXISTS groups_messages_likes (
            messageId INTEGER,
            studentId INTEGER,
            PRIMARY KEY(messageId, studentId)
        );
        
        CREATE TABLE IF NOT EXISTS groups_join_requests (
            groupId INTEGER,
            studentId INTEGER,
            motivation VARCHAR(1000),
            PRIMARY KEY(groupId, studentId)
        );
        
        CREATE TABLE IF NOT EXISTS chat_messages (
            id INTEGER AUTO_INCREMENT,
            studentId INTEGER,
            message VARCHAR(250),
            date BIGINT,
            PRIMARY KEY(id)
        );
    `;

    dbConnection.query(query, (err, result) => {
        if (err) throw err;
        console.log('Initialized MySQL tables');

        // setup routes
        app.use('/api', require('./routes'));
        console.log('Set routes up');

        // setup server
        app.listen(SERVER_PORT, () => console.log('Server started on port ' + SERVER_PORT));
    });
});

// export module
module.exports = app;
