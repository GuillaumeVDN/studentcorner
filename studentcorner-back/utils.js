const bcrypt = require("bcrypt");

module.exports = {

    // -------------------------------------------------- RESULT --------------------------------------------------

    resValue(res, val = {}) {
        res.json({ 'value': val });
    },
    resError(res, code = 500, err = 'unknown error') {
        res.status(code).json({ 'error': err });
    },

    // -------------------------------------------------- REQUEST --------------------------------------------------

    reqRequireParams(req, res, names, callback) {
        let args = [ req, res, req.body ];
        for (let i = 2; i < arguments.length; ++i) {
            args.push(arguments[i]);
        }
        this.reqRequireParamsIn.apply(this, args);
    },
    reqRequireParamsIn(req, res, lookIn, names, callback) {
        let values = [];
        for (let i = 3; i + 1 < arguments.length; ++i) {
            let name = arguments[i];
            let value = lookIn[name];
            if (value === undefined || value === null) {
                this.resError(res, 422, 'invalid request : missing ' + name);
                return;
            } else {
                values.push(value);
            }
        }
        arguments[arguments.length - 1].apply(null, values);
    },
    reqRequireAuthentication(req, res, callbackSuccess, callbackFail) {
        this.reqRequireParamsIn(req, res, req.headers, 'authorization', authorization => {
            let query = `SELECT * FROM students s
                         JOIN students_auth a ON studentId = s.id
                         WHERE a.token = '${authorization}';
            `;
            return this.dbQueryResultFirst(res, query, student => {
                if (callbackSuccess) callbackSuccess(student, authorization);
            },() => {
                if (callbackFail) callbackFail();
                else this.resError(res, 401, 'invalid token');
            });
        });
    },

    // -------------------------------------------------- DATABASE --------------------------------------------------

    dbQueryUpdate(res, query, callbackSuccess, callbackFail) {
        dbConnection.query(query, error => {
            if (error) {
                this.resError(res, 500, error.message)
                if (callbackFail) callbackFail();
            } else {
                if (callbackSuccess) callbackSuccess();
            }
        });
    },
    dbQueryResult(res, query, callbackSuccess, callbackFail) {
        dbConnection.query(query, (error, result) => {
            if (error) {
                this.resError(res, 500, error.message)
                if (callbackFail) callbackFail();
            } else {
                if (callbackSuccess) callbackSuccess(result);
            }
        });
    },
    dbQueryResultFirst(res, query, callbackSuccess, callbackFail) {
        this.dbQueryResult(res, query, result => {
            if (result.length >= 1) {
                callbackSuccess(result[0]);
            } else {
                if (callbackFail) callbackFail();
                else this.resError(res, 404, 'not found');
            }
        }, callbackFail);
    },

    // -------------------------------------------------- BCRYPT --------------------------------------------------

    hash(res, str, callback) {
        bcrypt.hash(str, 10, (err, hashed) => {
            console.log(err);
            if (err) this.resError(res);
            else callback(hashed);
        });
    },
    requireValidHash(res, plain, hashed, callback) {
        bcrypt.compare(plain, hashed, (err, result) => {
            if (err) this.resError(res);
            else if (result === true) callback();
            else this.resError(res, 401, 'invalid credentials');
        });
    }

};
